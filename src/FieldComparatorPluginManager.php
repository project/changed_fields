<?php

namespace Drupal\changed_fields;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Implementation of a field comparator plugin manager.
 */
class FieldComparatorPluginManager extends DefaultPluginManager {

  /**
   * FieldComparatorPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler instance to use.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/FieldComparator', $namespaces, $module_handler);
    $this->alterInfo('changed_fields_field_comparators_info');
    $this->setCacheBackend($cache_backend, 'changed_fields_plugins');
  }

}
