<?php

namespace Drupal\changed_fields;

/**
 * Extended observer interface.
 */
interface ObserverInterface extends \SplObserver {

  /**
   * Returns info for field comparator plugins.
   *
   * Info is an associative array of entity types with their bundles
   * and fields for watching.
   *
   * @return array
   *   Array of entity types/bundles with field names for watching.
   */
  public function getInfo();

}
